#!/bin/bash
###################################################################### 
#Copyright (C) 2019  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

cat << EOF
Please do not use this for illegal activities.
EOF


site="https://ww2.watch-series.co"

echo -n "Search: "
read q

output="$(wget -qO- "$site/search.html?keyword=$q")"

select="$(echo "$output"|grep 'href="/series/'|cut -d\" -f6|sort -u|fzf)"

  echo "$select selected"

  url="${site}$(echo "$output"|grep "$select"|head -n1|cut -d\" -f2)"
  echo "${url}"
  output="$(wget -qO- "${url}")"

#check if it is a movie or show
season="$(basename $url)/season"
echo "$output"|grep "$season" > /dev/null

o="$?"

if [ "$o" = "0" ]
then
  type="tv"
  echo "TV Series"
else
  type="movie"
  echo "Movie"
  fi

  function download(){
    l=$1
    echo "Checking $l for video links..."
    url="$(wget -qO- "${site}/${l}"|grep "openload.co"|cut -d\" -f6)"
    if [ "$url" = "" ]
    then
      echo "No Openload"
      #exit 1
      url="$(wget -qO- "${site}/${l}"|grep "streamango.com"|cut -d\" -f6)"
    fi
    echo "$url"
    title="$(echo "${l}"|cut -d\/ -f3).mp4"
    axel -n 10 "$(youtube-dl -g "$url")" -o "$title"

}


#####Start TV Series Section
#try and download the season
if [ "$type" = "tv" ]
then
  echo "Getting info for series"
  echo "${site}/seires/$season"
  wget "${site}/series/$season" -qO-|\
    grep vid_info|\
    cut -d\" -f4|\
    tac -|\
    while read l;
    do 
      echo "===$l==="
      download "$l"
    done
elif [ "$type" = "movie" ]
then
  l="series/$(basename $url)-episode-0"
  download "$l"
fi


